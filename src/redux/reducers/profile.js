import {
	PROFILE_REQUEST_SUCCESS,
	PROFILE_REQUEST_FAILURE,
	PROFILE_REQUEST,
} from '../actions/profileActions'

export const initialState = {
	data: null,
	errorMsg: '',
	isLoading: false,
}

export default (state = initialState, action) => {
	switch (action.type) {
		case PROFILE_REQUEST:
			return {
				...state,
				isLoading: true,
			}
		case PROFILE_REQUEST_SUCCESS:
			return {
				...state,
				errorMsg: '',
				data: action.payload,
				isLoading: false,
			}
		case PROFILE_REQUEST_FAILURE:
			return {
				...state,
				data: null,
				errorMsg: action.payload.errorMsg,
				isLoading: false,
			}
		default:
			return state
	}
}
