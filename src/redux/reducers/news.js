import {
	NEWS_REQUEST_SUCCESS,
	NEWS_REQUEST_FAILURE,
	NEWS_REQUEST,
} from '../actions/newsActions'

export const initialState = {
	allNews: null,
	errorMsg: '',
	isLoading: false,
}

export default (state = initialState, action) => {
	switch (action.type) {
		case NEWS_REQUEST:
			return {
				...state,
				isLoading: true,
			}
		case NEWS_REQUEST_SUCCESS:
			return {
				...state,
				errorMsg: '',
				allNews: action.payload,
				isLoading: false,
			}
		case NEWS_REQUEST_FAILURE:
			return {
				...state,
				allNews: null,
				errorMsg: action.payload.errorMsg,
				isLoading: false,
			}
		default:
			return state
	}
}
