import { combineReducers } from 'redux'
import session from '../reducers/session'
import profile from '../reducers/profile'
import news from '../reducers/news'

export default combineReducers({
  session,
  profile,
  news,
})