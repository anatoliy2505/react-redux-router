import reducer, { initilState } from '../reducers/session'
import {
	LOG_IN,
	LOG_OUT,
	LOG_IN_FAIL,
	REMOVE_ERROR_MSG,
	LOG_IN_STARTED,
} from '../actions/sessionActions'

describe('>>> sessionReducer test', () => {
	it('LOG_IN_STARTED', () => {
		const action = {
			type: LOG_IN_STARTED,
		}
		expect(reducer(initilState, action)).toEqual({
			userId: null,
			errorMsg: '',
			logInStarted: true,
		})
	})

	it('LOG_IN', () => {
		const stateBefore = {
			userId: null,
			errorMsg: '',
			logInStarted: true,
		}
		const action = {
			type: LOG_IN,
			payload: 1,
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			userId: action.payload,
			logInStarted: false,
		})
	})

	it('LOG_IN_FAIL', () => {
		const stateBefore = {
			userId: null,
			errorMsg: '',
			logInStarted: true,
		}
		const action = {
			type: LOG_IN_FAIL,
			payload: {
				errorMsg: '... error',
			},
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			errorMsg: action.payload.errorMsg,
			logInStarted: false,
		})
	})

	it('LOG_OUT', () => {
		const stateBefore = {
			userId: 1,
			errorMsg: '',
			logInStarted: false,
		}
		const action = {
			type: LOG_OUT,
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			userId: null,
			logInStarted: false,
		})
	})

	it('REMOVE_ERROR_MSG', () => {
		const stateBefore = {
			userId: null,
			errorMsg: '... error',
			logInStarted: false,
		}
		const action = {
			type: REMOVE_ERROR_MSG,
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			errorMsg: '',
		})
	})
})
