import reducer, { initilState } from '../reducers/profile'
import {
	PROFILE_REQUEST_SUCCESS,
	PROFILE_REQUEST_FAILURE,
	PROFILE_REQUEST,
} from '../actions/profileActions'

describe('>>> profileReducer test', () => {
	it('PROFILE_REQUEST', () => {
		const action = {
			type: PROFILE_REQUEST,
		}
		expect(reducer(initilState, action)).toEqual({
			data: null,
			errorMsg: '',
			isLoading: true,
		})
	})

	it('PROFILE_REQUEST_SUCCESS', () => {
		const stateBefore = {
			data: null,
			errorMsg: '',
			isLoading: true,
		}
		const action = {
			type: PROFILE_REQUEST_SUCCESS,
			payload: [1, 2, 3],
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			data: action.payload,
			isLoading: false,
		})
	})

	it('PROFILE_REQUEST_FAILURE', () => {
		const stateBefore = {
			data: null,
			errorMsg: '',
			isLoading: true,
		}
		const action = {
			type: PROFILE_REQUEST_FAILURE,
			payload: {
				errorMsg: '... error',
			},
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			errorMsg: action.payload.errorMsg,
			isLoading: false,
		})
	})
})
