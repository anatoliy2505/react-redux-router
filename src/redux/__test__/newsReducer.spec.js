import reducer, { initilState } from '../reducers/news'
import {
	NEWS_REQUEST_SUCCESS,
	NEWS_REQUEST_FAILURE,
	NEWS_REQUEST,
} from '../actions/newsActions'

describe('>>> newsReducer test', () => {
	it('NEWS_REQUEST', () => {
		const action = {
			type: NEWS_REQUEST,
		}
		expect(reducer(initilState, action)).toEqual({
			allNews: null,
			errorMsg: '',
			isLoading: true,
		})
	})

	it('NEWS_REQUEST_SUCCESS', () => {
		const action = {
			type: NEWS_REQUEST_SUCCESS,
			payload: [1, 2, 3],
		}
		const stateBefore = {
			allNews: null,
			errorMsg: '',
			isLoading: true,
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			allNews: action.payload,
			isLoading: false,
		})
	})

	it('NEWS_REQUEST_FAILURE', () => {
		const action = {
			type: NEWS_REQUEST_FAILURE,
			payload: {
				errorMsg: 'dafdfadf',
			},
		}
		const stateBefore = {
			allNews: null,
			errorMsg: '',
			isLoading: true,
		}
		expect(reducer(stateBefore, action)).toEqual({
			...stateBefore,
			errorMsg: action.payload.errorMsg,
			isLoading: false,
		})
	})
})
