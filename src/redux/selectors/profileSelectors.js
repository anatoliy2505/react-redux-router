import { createSelector } from 'reselect'

export const profileRootSelector = state => state.profile

export const profileIsLoadingSelector = createSelector(
	profileRootSelector,
	state => Boolean(state && state.isLoading)
)

export const profileErrorMsgSelector = createSelector(
	profileRootSelector,
	state => (state && state.errorMsg) || ''
)

export const profileDataSelector = createSelector(
	profileRootSelector,
	state => (state && state.data) || null
)
