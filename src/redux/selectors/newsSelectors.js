import { createSelector } from 'reselect'

export const newsRootSelector = state => state.news

export const newsIsLoadingSelector = createSelector(newsRootSelector, state =>
	Boolean(state && state.isLoading)
)

export const newsErrorMsgSelector = createSelector(
	newsRootSelector,
	state => (state && state.errorMsg) || ''
)

export const newsDataSelector = createSelector(
	newsRootSelector,
	state => (state && state.allNews) || null
)
