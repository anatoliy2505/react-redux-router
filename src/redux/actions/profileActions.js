import { errorLog } from './constants'
import { getProfileData } from '../../utils/api/profile'

export const PROFILE_REQUEST_SUCCESS = 'PROFILE_REQUEST_SUCCESS'
export const PROFILE_REQUEST_FAILURE = 'PROFILE_REQUEST_FAILURE'
export const PROFILE_REQUEST = 'PROFILE_REQUEST'

export function getProfileUser(id) {
	return dispatch => {
		dispatch({
			type: PROFILE_REQUEST,
		})
		getProfileData(id)
			.then(res => {
				if (res.status === 'ok') {
					dispatch({
						type: PROFILE_REQUEST_SUCCESS,
						payload: res.data,
					})
				} else {
					dispatch({
						type: PROFILE_REQUEST_FAILURE,
						payload: {
							errorMsg: errorLog[res.message],
						},
						error: true, // https://github.com/redux-utilities/flux-standard-action
					})
				}
			})
			.catch(error => {
				dispatch({
					type: PROFILE_REQUEST_FAILURE,
					payload: {
						errorMsg: errorLog.server_connection_failed,
					},
					error: true, // https://github.com/redux-utilities/flux-standard-action
				})
				console.warn('getProfileUser error ', error)
			})
	}
}
