import { errorLog } from './constants'
import { getAllNewsData } from '../../utils/api/news'

export const NEWS_REQUEST_SUCCESS = 'NEWS_REQUEST_SUCCESS'
export const NEWS_REQUEST_FAILURE = 'NEWS_REQUEST_FAILURE'
export const NEWS_REQUEST = 'NEWS_REQUEST'

export function getAllNews() {
	return dispatch => {
		dispatch({
			type: NEWS_REQUEST,
		})
		getAllNewsData()
			.then(res => {
				if (res.status === 'ok') {
					dispatch({
						type: NEWS_REQUEST_SUCCESS,
						payload: res.data,
					})
				} else {
					console.log(errorLog[res.message])
					dispatch({
						type: NEWS_REQUEST_FAILURE,
						payload: {
							errorMsg: errorLog[res.message],
						},
						error: true,
					})
				}
			})
			.catch(error => {
				dispatch({
					type: NEWS_REQUEST_FAILURE,
					payload: {
						errorMsg: errorLog.server_connection_failed,
					},
					error: true,
				})
				console.warn('getAllNews error ', error)
			})
	}
}
