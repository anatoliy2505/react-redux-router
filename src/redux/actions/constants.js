export const errorLog = {
    wrong_email_or_password: 'Invalid login or password',
    server_connection_failed: 'Server connection failed',
    user_not_found: 'Error. Your information not found',
    news_not_found: 'News not found',
}