import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Loading, FieldItem } from '../../components'
import { logIn, removeErorroMsg } from '../../redux/actions/sessionActions'
import {
	sessionErrorMsgSelector,
	sessionLogInStartedSelector,
} from '../../redux/selectors/sessionSelectors'

import { removeErrorOfField } from '../../utils/helpers'
import validateFields from '../../utils/helpers/validateFields'

const Login = ({ errorMsg, isLoading, removeErorroMsg, logIn }) => {
	const [state, setState] = useState({
		email: '',
		password: '',
		errors: null,
	})

	useEffect(() => {
		if (errorMsg) {
			setState(prev => ({
				...prev,
				password: '',
			}))
		}
	}, [errorMsg])

	const onChangeField = e => {
		const fieldName = e.currentTarget.name,
			value = e.currentTarget.value,
			newError = removeErrorOfField(fieldName, state.errors)

		if (errorMsg) {
			removeErorroMsg()
		}

		setState(prev => ({
			...prev,
			[fieldName]: value,
			errors: newError,
		}))
	}

	const onSubmit = e => {
		e.preventDefault()
		const { email, password } = state,
			values = { email, password },
			errors = {}
		validateFields(errors, values)
		if (Object.keys(errors).length !== 0) {
			setState(prev => ({
				...prev,
				errors,
			}))
		} else {
			logIn({
				email,
				password,
			})
		}
	}

	const { email, password, errors } = state,
		disabled = email.length && password.length ? false : true

	return (
		<>
			<h1 className="title">Inter</h1>
			{isLoading ? (
				<Loading />
			) : (
				<form className="login-form" onSubmit={onSubmit}>
					{!errorMsg ? null : <div className="error-msg">*{errorMsg}</div>}
					<FieldItem
						type={'text'}
						name={'email'}
						error={errors && errors.email}
						className={'login-form__field'}
						onChangeField={onChangeField}
						autoComplete={'off'}
						value={email}
					/>
					<FieldItem
						type={'password'}
						name={'password'}
						error={errors && errors.password}
						className={'login-form__field'}
						onChangeField={onChangeField}
						autoComplete={'off'}
						value={password}
					/>
					<button
						type={'submit'}
						className={'login-form__button'}
						disabled={disabled || isLoading}
					>
						{!disabled ? 'Log In' : 'Fill in the fields'}
					</button>
				</form>
			)}
		</>
	)
}

export default connect(
	state => ({
		errorMsg: sessionErrorMsgSelector(state),
		isLoading: sessionLogInStartedSelector(state),
	}),
	{ logIn, removeErorroMsg }
)(Login)

Login.propTypes = {
	errorMsg: PropTypes.string.isRequired,
	isLoading: PropTypes.bool.isRequired,
	location: PropTypes.object.isRequired,
	removeErorroMsg: PropTypes.func.isRequired,
	logIn: PropTypes.func.isRequired,
}
