import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Loading } from '../../components'
import { getAllNews } from '../../redux/actions/newsActions'
import {
	newsIsLoadingSelector,
	newsErrorMsgSelector,
	newsDataSelector,
} from '../../redux/selectors/newsSelectors'

import './News.scss'

const News = ({ allNews, errorMsg, isLoading, getAllNews }) => {
	useEffect(() => {
		if (!allNews) {
			getAllNews()
		}
	}, [allNews, getAllNews])

	const newsList = items =>
		items.map(({ id, title, text }) => (
			<div key={id} className={'news-item'}>
				<h2 className="news-item__title">{title}</h2>
				<div className="news-item__text">{text}</div>
			</div>
		))

	return (
		<>
			<h1 className={'title'}>News</h1>
			{!allNews && isLoading ? (
				<Loading />
			) : errorMsg ? (
				<h1>{errorMsg}</h1>
			) : allNews && allNews.length ? (
				<>
					<div className="news-container">{newsList(allNews)}</div>
					<div className="news-count">Total news: {allNews.length}</div>
				</>
			) : null}
		</>
	)
}

export default connect(
	state => ({
		allNews: newsDataSelector(state),
		errorMsg: newsErrorMsgSelector(state),
		isLoading: newsIsLoadingSelector(state),
	}),
	{ getAllNews }
)(News)

News.propTypes = {
	allNews: PropTypes.oneOfType([
		PropTypes.array.isRequired,
		PropTypes.oneOf([null]).isRequired,
	]),
	errorMsg: PropTypes.string.isRequired,
	isLoading: PropTypes.bool.isRequired,
	getAllNews: PropTypes.func,
}
