import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Loading } from '../../components'
import { sortLinks } from '../../utils/helpers'
import { getProfileUser } from '../../redux/actions/profileActions'
import {
	profileIsLoadingSelector,
	profileErrorMsgSelector,
	profileDataSelector,
} from '../../redux/selectors/profileSelectors'
import { sessionUserIdSelector } from '../../redux/selectors/sessionSelectors'

import './Profile.scss'

const Profile = ({ profileId, data, getProfileUser, errorMsg, isLoading }) => {
	useEffect(() => {
		if (!data) {
			getProfileUser(profileId)
		}
	}, [data, profileId, getProfileUser])

	return (
		<>
			<h1 className={'title'}>It is Your Profile</h1>
			{!data && isLoading ? (
				<Loading />
			) : errorMsg ? (
				<h1 className={'error-message'}>{errorMsg}</h1>
			) : data ? (
				<>
					<div className="wrap-user-info">
						<h3 className={'label'}>City: {data.city}</h3>
					</div>

					<div className="wrap-user-info">
						<h3 className={'label'}>Knowledge of languages: </h3>
						<ul className={'languges list'}>
							{data.languages.map(item => (
								<li className={'languges__item'} key={item}>
									{item}
								</li>
							))}
						</ul>
					</div>

					<div className="wrap-user-info">
						<h3 className={'label'}>Links: </h3>
						<ul className={'social list'}>
							{sortLinks(data.social).map(item => (
								<li className={'social__item'} key={item.label}>
									<a href={item.link} target={'_blank'}>
										<img
											src={`${process.env.PUBLIC_URL}/${item.label}.png`}
											alt={item.label}
										/>
									</a>
								</li>
							))}
						</ul>
					</div>
				</>
			) : null}
		</>
	)
}

export default connect(
	state => ({
		profileId: sessionUserIdSelector(state),
		data: profileDataSelector(state),
		errorMsg: profileErrorMsgSelector(state),
		isLoading: profileIsLoadingSelector(state),
	}),
	{ getProfileUser }
)(Profile)

Profile.propTypes = {
	profileId: PropTypes.number.isRequired,
	data: PropTypes.oneOfType([
		PropTypes.shape({
			userId: PropTypes.number,
			city: PropTypes.string,
			languages: PropTypes.array,
			social: PropTypes.array,
		}).isRequired,
		PropTypes.oneOf([null]).isRequired,
	]),
	errorMsg: PropTypes.string.isRequired,
	isLoading: PropTypes.bool.isRequired,
	getProfileUser: PropTypes.func.isRequired,
}
