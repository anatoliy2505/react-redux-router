import { Home } from './Home';
import Login from './Login';
import News from './News';
import Profile from './Profile';

export const routes = [
    {
        path: ['/','/home'],
        name: 'Home',
        component: Home,
        isExact: true,
        isNavbar: true,
        isOne: false
    },
    {
        path: '/news',
        name: 'News',
        component: News,
        isNavbar: true,
        isOne: true
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
        isPrivate: true,
        isNavbar: true,
        isOne: true
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        isNavbar: true,
        isOne: true
    }
];
