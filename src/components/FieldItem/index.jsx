import React from 'react'
import './FieldItem.scss'

const FieldItem = ({ onChangeField, error, ...res }) => {

	if(error) {
		return (
			<div className={'error__wrapper'}>
				<input { ...res } onChange={onChangeField}/>
				{error ? <p className={'error__text'}>{ error }</p> : null}
			</div>
		)
	}
	return (
		<input { ...res } onChange={onChangeField}/>
	)
}

export default FieldItem