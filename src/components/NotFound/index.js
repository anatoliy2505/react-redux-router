import React from 'react'

import './NotFound.scss'

const NotFound = () => {

    return (
        <div className={'not-found'}>
            <h1>
                Страница не найдена
            </h1>
        </div>
    )
}

export default NotFound