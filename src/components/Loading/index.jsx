import React from 'react'

import loadingImg from './img/loading.gif'
import './Loading.scss'

const Loading = () => {
    return (
        <div className={'loading__container'}>
            <img className={'loading__image'} src={loadingImg} alt='loading'/>
        </div>
    )
}

export default Loading