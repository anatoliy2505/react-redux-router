import React from 'react';
import './Page.scss';


const Page = props => {
    return (
        <div className="container">
            <div className="page">
                {props.children}
            </div>            
        </div>
    )
}

export default Page;