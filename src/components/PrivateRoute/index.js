import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { sessionIsAuthSelector } from '../../redux/selectors/sessionSelectors'

const PrivateRoure = ({ component: Component, isAuth, ...rest }) => {
	return (
		<Route
			{...rest}
			render={props =>
				isAuth ? <Component {...props} /> : <Redirect to={'/login'} />
			}
		/>
	)
}

export default connect(
	state => ({
		isAuth: sessionIsAuthSelector(state),
	}),
	{}
)(PrivateRoure)

PrivateRoure.propTypes = {
	component: PropTypes.object.isRequired,
	isAuth: PropTypes.bool.isRequired,
}
