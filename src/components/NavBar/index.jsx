import React from 'react'
import { NavLink  } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { logOut } from '../../redux/actions/sessionActions'
import { sessionIsAuthSelector } from '../../redux/selectors/sessionSelectors'

import './NavBar.scss'

const NavBar = ({ routes, isAuth, logOut }) => {

    const createItems = routes.map(route => {
            if( route.path === '/login' && isAuth) {
                return <li key={'logout'}>
                            <NavLink className={"main-nav__item"} onClick={logOut} to={route.path}>
                                Logout
                            </NavLink >
                        </li>
            }
            return <li key={route.path}>
                        <NavLink className={"main-nav__item"} to={ !route.isOne ? route.path['1'] : route.path }>
                            {route.name}
                        </NavLink >
                    </li>
    })

    return (
        <header>
            <ul className={"main-nav"}>
                { createItems }
            </ul>
        </header>
    )
}

export default connect(state => (
    {
        isAuth: sessionIsAuthSelector(state),
    }
),{logOut})(NavBar)

NavBar.propTypes = {
    isAuth: PropTypes.bool.isRequired,
    routes: PropTypes.array.isRequired,
    logOut: PropTypes.func.isRequired,
}