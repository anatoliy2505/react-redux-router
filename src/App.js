import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { Page, PrivateRoute, NavBar, NotFound } from './components'
import { routes } from './pages/routes'
import { sessionIsAuthSelector } from './redux/selectors/sessionSelectors'

const App = ({ isAuth }) => {
	const routersSwitch = () => (
		<Switch>
			{isAuth ? (
				<Route
					exact
					path={'/login'}
					render={() => <Redirect to={'/profile'} />}
				/>
			) : null}
			{routes.map(route =>
				route.isPrivate ? (
					<PrivateRoute
						key={route.path}
						exact={route.isExact}
						path={route.path}
						component={route.component}
					/>
				) : (
					<Route
						key={route.path}
						exact={route.isExact}
						path={route.path}
						component={route.component}
					/>
				)
			)}
			<Route component={NotFound} />
		</Switch>
	)

	return (
		<Page>
			<NavBar routes={routes.filter(route => route.isNavbar)} />
			{routersSwitch()}
		</Page>
	)
}

export default connect(
	state => ({
		isAuth: sessionIsAuthSelector(state),
	}),
	{}
)(App)
