export default  (errors, values) => {

	const rules = {
		email: (value) => {
			if (!value) {
					errors.email = 'Enter Your E-Mail'
			} else if (
					!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(
							value.trim()
					)
			) {
					errors.email = 'Incorrect E-Mail'
			}
		},
		password: (value) => {
			if (!value) {
					errors.password = 'Enter Your password'

			} else if ( value.trim().length < 3 ) {
					errors.password = 'More than three characters'
			}
		},
	}

	Object.keys(values).forEach(
		key => rules[key] && rules[key](values[key])
	)
}