export const removeErrorOfField = (name, errors) => {
	if(errors) {
		if(errors[name]) {
			delete errors[name]					
		} 
		return errors
	}
	return null
}

export const sortLinks = (links, field = 'web') => {
	return links.filter(link => link.label === field).concat(links.filter(link => link.label !== field))
}
