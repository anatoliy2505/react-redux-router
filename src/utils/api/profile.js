import { ROOT_URL } from './constants'

export const getProfileData = ( id ) => {
  return fetch(`${ROOT_URL}/user-info/${id}`)
    .then(response => {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error(response.status)
      }
  }).then(data => data)
}