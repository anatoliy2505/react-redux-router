import { ROOT_URL } from './constants'

export const getAllNewsData = () => {
  return fetch(`${ROOT_URL}/news`)
    .then(response => {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error(response.status)
      }
  }).then(data => data)
}