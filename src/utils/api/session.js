import { ROOT_URL } from './constants'

export const postLogInfo = ({ email, password }) => {
    return fetch(`${ROOT_URL}/validate`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email,
            password
        }) 
    }).then(response => {
        if (response.ok) {
          return response.json()
        } else {
          throw new Error(response.status)
        }
    }).then(data => data)
}